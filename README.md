# Vue Netcore

1. Run `docker-compose up --build` and you'll have 4 running containers
   1. Postrgre DB 
   2. Pg MyAdmin [http://localhost:5050](http://localhost:5050)
   3. Front app based on [Vue Argon Dashboard Asp.net](https://www.creative-tim.com/product/vue-argon-dashboard-asp-net) - http://localhost:8080/
   4. Front app based on [Vue Armour](https://github.com/Armour/vue-typescript-admin-template)  - http://localhost:9527/
   5. An Identity server - http://localhost:5000/
2. Install Entity Framework Core tools `dotnet tool install --global dotnet-ef`
3. Go to `IndentityServer` folder and run `dotnet-ef database update`
4. To feed the database, call the endpoint http://localhost:5000/api/test/feed



You now can access to Postrgre database through PgAdmin : http://localhost:5050/