﻿using System.Linq;
using System.Threading.Tasks;
using IdentityServer.Filters;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace IdentityServer.Controllers
{
    [SecurityHeaders]
    public class PasswordController : Controller
    {
        private readonly UserManager<ApplicationUser> userManager;

        public PasswordController(UserManager<ApplicationUser> userManager)
        {
            this.userManager = userManager;
        }

        [HttpPost]
        public async Task<IActionResult> Index([FromBody] UpdatePasswordModel updatePasswordModel)
        {

            var user = await userManager.FindByNameAsync(updatePasswordModel.UserName);

            if (user == null)
                return NotFound("User not found");

            var result = await userManager.ChangePasswordAsync(user, updatePasswordModel.CurrentPassword, updatePasswordModel.NewPassword);

            if (result.Succeeded)
            {
                return Ok();
            }

            return BadRequest(result.Errors.Select(s => s.Description));
        }
    }
}