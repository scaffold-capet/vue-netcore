﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer.Filters;
using IdentityServer.Models;
using IdentityServer.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace IdentityServer.Controllers
{
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly SeedDataService seedDataService;

        public TestController(
            IConfiguration configuration,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            this.configuration = configuration;
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.seedDataService = new SeedDataService(userManager, roleManager);
        }

        [HttpGet]
        [Route("api/test/feed")]
        public async Task<IActionResult> EnsureData()
        {
            seedDataService.EnsureData();
            return new OkResult();
        }

        [HttpGet]
        [Authorize]
        [Route("transactions")]
        public async Task<IActionResult> GetTransactions()
        {
            var transactionList = new List<TransactionModel>
            {
                new(Guid.NewGuid(), "success", DateTime.Now, "fakeName1", 1.2f),
                new(Guid.NewGuid(), "pending", DateTime.Now, "fakeName2", 3.4f),
                new(Guid.NewGuid(), "success", DateTime.Now, "fakeName3", 5.6f),
                new(Guid.NewGuid(), "pending", DateTime.Now, "fakeName4", 7.8f),
                new(Guid.NewGuid(), "success", DateTime.Now, "fakeName5", 9.10f),
                new(Guid.NewGuid(), "pending", DateTime.Now, "fakeName6", 11.12f),
                new(Guid.NewGuid(), "pending", DateTime.Now, "fakeName6", 11.12f),
                new(Guid.NewGuid(), "pending", DateTime.Now, "fakeName6", 11.12f),
                new(Guid.NewGuid(), "pending", DateTime.Now, "fakeName6", 11.12f),
                new(Guid.NewGuid(), "pending", DateTime.Now, "fakeName6", 11.12f),
                new(Guid.NewGuid(), "pending", DateTime.Now, "fakeName6", 11.12f),
                new(Guid.NewGuid(), "pending", DateTime.Now, "fakeName6", 11.12f),
                new(Guid.NewGuid(), "pending", DateTime.Now, "fakeName6", 11.12f)
            };
            return new OkObjectResult(transactionList);
        }

        [HttpGet]
        [Route("all")]
        public IActionResult GetAll()
        {
            return new OkObjectResult(userManager.Users);
        }

        [HttpGet]
        [Route("user")]
        [Authorize("member")]
        public async Task<IActionResult> GetUser()
        {
            return new OkObjectResult(await userManager.GetUsersInRoleAsync("member"));
        }

        [HttpGet]
        [Route("mod")]
        public async Task<IActionResult> GetMod()
        {
            return new OkObjectResult(await userManager.GetUsersInRoleAsync("mod"));
        }

        [HttpGet]
        [Route("admin")]
        [SecurityHeaders]
        public async Task<IActionResult> GetAdmin()
        {
            return new OkObjectResult(await userManager.GetUsersInRoleAsync("admin"));
        }
    }
}