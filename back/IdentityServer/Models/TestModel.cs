﻿using System;

namespace IdentityServer.Models
{
    public class TransactionModel
    {
        public Guid OrderId { get; set; }
        public string Status{ get; set; }
        public DateTime Timestamp{ get; set; }
        public string Username{ get; set; }
        public float Price{ get; set; }

        public TransactionModel(Guid orderId, string status, DateTime timestamp, string username, float price)
        {
            OrderId = orderId;
            Status = status;
            Timestamp = timestamp;
            Username = username;
            Price = price;
        }
    }
}