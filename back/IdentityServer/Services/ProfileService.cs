﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer.Models;
using IdentityServer4.Extensions;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;

namespace IdentityServer.Services
{
    public class ProfileService : IProfileService
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory;
        private readonly RoleManager<IdentityRole> roleManager;

        public ProfileService(UserManager<ApplicationUser> userManager,
            IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory,
            RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.claimsFactory = claimsFactory;
            this.roleManager = roleManager;
        }

        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await userManager.FindByIdAsync(sub);

            var principal = await claimsFactory.CreateAsync(user);

            var claims = principal.Claims.ToList();

            claims = claims.Where(claim => context.RequestedClaimTypes.Contains(claim.Type)).ToList();

            claims.Add(new Claim("userName", user.UserName ?? string.Empty));
            
            // var roleClaim = await userManager.GetClaimsAsync(user);
            // claims.Add(new Claim("Roles", roleClaim.First(p=>p.Type =="Roles").Value ?? string.Empty));

            if (userManager.SupportsUserRole)
            {
                var roles = await userManager.GetRolesAsync(user);
                foreach (var role in roles)
                {
                    claims.Add(new Claim(JwtClaimTypes.Role, role));
                    if (roleManager.SupportsRoleClaims)
                    {
                        var identityRole = await roleManager.FindByNameAsync(role);
                        if (identityRole != null)
                        {
                            claims.AddRange(await roleManager.GetClaimsAsync(identityRole));
                        }
                    }
                }
            }

            context.IssuedClaims = claims;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            var sub = context.Subject.GetSubjectId();
            var user = await userManager.FindByIdAsync(sub);
            context.IsActive = user != null;
        }
    }
}