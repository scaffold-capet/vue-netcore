﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using IdentityModel;
using IdentityServer.Models;
using Microsoft.AspNetCore.Identity;
using Serilog;

namespace IdentityServer.Services
{
    public class SeedDataService
    {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private const string CommonPassword = "Pass123$";
        private static readonly List<string> Users = new List<string> {"alice@email.com", "bob@email.com"};
        private static readonly List<string> RoleNames = new List<string> {"standard", "member", "admin"};

        public SeedDataService(
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        public void EnsureData()
        {
            CreateUsers();
            CreateRoles();
            AddClaims();
            AddRolesToEverybody();
        }

        private void CreateUsers()
        {
            foreach (var userName in Users)
            {
                var user = userManager.FindByNameAsync(userName).Result;
                if (user == null)
                {
                    user = new ApplicationUser
                    {
                        UserName = userName,
                        Email = userName,
                        EmailConfirmed = true,
                    };
                    var result = userManager.CreateAsync(user, CommonPassword).Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }

                    Log.Information($"{userName} user created");
                }
            }
        }

        private void CreateRoles()
        {
            foreach (var roleName in RoleNames)
            {
                var identityRole = roleManager.FindByNameAsync(roleName).Result;
                if (identityRole == null)
                {
                    identityRole = new IdentityRole(roleName);
                    var result = roleManager.CreateAsync(identityRole).Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }

                    Log.Information($"{roleName} role created");
                }
            }
        }

        private void AddClaims()
        {
            foreach (var userName in Users)
            {
                var user = userManager.FindByNameAsync(userName).Result;

                var name = userName.Split("@")[0];
                var userClaims = new List<Claim>
                {
                    new Claim(JwtClaimTypes.Name, $"{name}"),
                    new Claim(JwtClaimTypes.WebSite, $"http://{name}.com")
                };
                userClaims.AddRange(RoleNames.Select(p => new Claim("Roles", p)));

                var savedUserClaims = userManager.GetClaimsAsync(user).Result;
                foreach (var savedUserClaim in savedUserClaims)
                {
                    if (userClaims.Contains(savedUserClaim))
                    {
                        Log.Information($"{userName} already have claim {savedUserClaim.Type}:{savedUserClaim.Value}");
                        continue;
                    }

                    var result = userManager.AddClaimsAsync(user, userClaims).Result;
                    if (!result.Succeeded)
                    {
                        throw new Exception(result.Errors.First().Description);
                    }
                }
            }
        }

        public void AddRolesToEverybody()
        {
            var users = userManager.Users.ToList();
            foreach (var user in users)
            {
                AddRolesAsync(RoleNames, userManager, user);
            }
        }

        private void AddRolesAsync(List<string> roleNames, UserManager<ApplicationUser> userMgr,
            ApplicationUser user)
        {
            foreach (var roleName in roleNames)
            {
                var result = userMgr.AddToRoleAsync(user, roleName).Result;
            }
        }
    }
}