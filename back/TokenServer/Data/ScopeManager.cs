﻿using System.Collections.Generic;
using IdentityServer4.Models;

namespace TokenServer.Data
{
    public static class ScopeManager
    {
        public static IEnumerable<ApiScope> Scopes =>
            new List<ApiScope>
            {
                new ApiScope(name: "app.api.whatever.read",   displayName: "Read your data."),
                new ApiScope(name: "app.api.whatever.write",  displayName: "Write your data."),
                new ApiScope(name: "app.api.whatever.full", displayName: "Manage your data."),
                new ApiScope(name: "app.api.weather", displayName: "Manage api weather your data.")
            };
    }
}