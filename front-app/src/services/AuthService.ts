import axios from 'axios';
import ClientOAuth2 from 'client-oauth2';
import authHeader from './auth-header';

const VUE_APP_AUTH_SERVER_URL = 'http://localhost:5000';

const config = {
    clientId: "vue.client",
    accessTokenUri: `${VUE_APP_AUTH_SERVER_URL}/connect/token`,
    userInfoUri: `${VUE_APP_AUTH_SERVER_URL}/connect/userinfo`,
    endsessionUri: `${VUE_APP_AUTH_SERVER_URL}/connect/endsession`,
    introspectURL: `${VUE_APP_AUTH_SERVER_URL}/connect/introspect`,
    scopes: ["openid", "profile", "email"],
};

const oauth = new ClientOAuth2(config);

class AuthService {

    login(username: string, password: string) {

        return oauth.owner.getToken(username, password)
            .then((result) => {
                localStorage.setItem('token', result.accessToken);
                console.log(result.accessToken);
                return this.getUserInfo();
            })
            .catch((error) => {
                console.log("LogIn Error:" + error);
                return false;
            });
    }

    logout() {
        localStorage.removeItem('token');
    }

    register(name: string, email: string, password: string, confirmPassword: string) {
        return axios.post(VUE_APP_AUTH_SERVER_URL + '/Register', {
            name,
            email,
            password,
            confirmPassword,
            returnUrl : "/login"
        });
    }

    getUserInfo() {
        return axios.get(config.userInfoUri, { headers: authHeader() }).then(result => {
            return result.data;
        })
    }
}

export default new AuthService();