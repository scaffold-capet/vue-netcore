export default function authHeader() {
    const userToken = localStorage.getItem('token');
    let token = userToken ? userToken : "";
    if (token) {
      return { Authorization: 'Bearer ' + token };
    } else {
      return {};
    }
  }