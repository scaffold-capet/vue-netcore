import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators';
import AuthService from '@/services/AuthService';

const userToken = localStorage.getItem('token');

@Module({ namespaced: true })
class User extends VuexModule {
  public status = userToken ? { loggedIn: true } : { loggedIn: false };
  public user = userToken ? userToken : null;

  @Mutation
  public loginSuccess(user: any): void {
    this.status.loggedIn = true;
    this.user = user;
  }

  @Mutation
  public loginFailure(): void {
    this.status.loggedIn = false;
    this.user = null;
  }

  @Mutation
  public logout(): void {
    this.status.loggedIn = false;
    this.user = null;
  }

  @Mutation
  public registerSuccess(): void {
    this.status.loggedIn = false;
  }

  @Mutation
  public registerFailure(): void {
    this.status.loggedIn = false;
  }

  @Action({ rawError: true })
  login(data: any): Promise<any> {
    
    return AuthService.login(data.username, data.password).then(
      result => {
        console.log(result);

        if(result){
          this.context.commit('loginSuccess', result);
          return Promise.resolve(result);
        }
        else{
          return Promise.reject("message");
        }
      },
      error => {
        this.context.commit('loginFailure');
        const message =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();
        return Promise.reject(message);
      }
    );
  }

  @Action
  signOut(): void {
    AuthService.logout();
    this.context.commit('logout');
  }

  @Action({ rawError: true })
  register(data: any): Promise<any> {
    return AuthService.register(data.username, data.email, data.password, data.confirmPassword).then(
      response => {
        this.context.commit('registerSuccess');
        return Promise.resolve(response.data);
      },
      error => {
        this.context.commit('registerFailure');
        console.log(error.response);
        const message =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();
        return Promise.reject(message);
      }
    );
  }

  get isLoggedIn(): boolean {
    return this.status.loggedIn;
  }
}

export default User;