import ClientOAuth2 from 'client-oauth2'

const config = {
  clientId: 'vue.admin.template',
  accessTokenUri: `${process.env.VUE_APP_BASE_API}/connect/token`,
  userInfoUri: `${process.env.VUE_APP_BASE_API}/connect/userinfo`,
  endsessionUri: `${process.env.VUE_APP_BASE_API}/connect/endsession`,
  introspectURL: `${process.env.VUE_APP_BASE_API}/connect/introspect`,
  scopes: ['openid', 'profile', 'email']
}

const oauth = new ClientOAuth2(config)

const token = (data: any): Promise<any> => {
  return oauth.owner.getToken(data.username, data.password)
}

export default token
